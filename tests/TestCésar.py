import unittest
from César import *


class Test_Unitaire(unittest.TestCase):

    def Test_Cryptage(self):
        self.assertEqual('YOU', Cryptage('^TZ',5))
        self.assertEqual('gMc', Cryptage('wWm',10))

    def Test_Decryptage(self):
        self.assertEqual('Smz', Decryptage('Lfs',7))
        self.assertEqual('Kxq', Decryptage('Ang',10))   
        