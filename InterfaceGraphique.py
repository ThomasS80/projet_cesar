from tkinter import * 
from César import *
from tkinter.messagebox import *

#Comment fonctionne le programme si on ecrit a gauche nous devons appuyer sur le bouton veux tu crypter pour crypter un text qui sera 
#envoyé a droite
#Si nous souhaitons decrypter nous devons envoyer le message a droite puis appuyer sur le bouton decrypter qui renverra a gauche 
#le mess decrypté

def button_Decryptage_callback(): #Il sert a appeler le bouton pour effectuer le Decryptage
    PasEncoreCrypt.delete("0.0",END)
    PasEncoreCrypt.insert("0.0", Decryptage(ApresCrypt.get("0.0", END), Nbr.get()))

def button_Cryptage_callback(): #Il sert a appeler le bouton pour effectuer le cryptage
    ApresCrypt.delete("0.0", END)
    ApresCrypt.insert("0.0", Cryptage(PasEncoreCrypt.get("0.0", END), Nbr.get()))

root = Tk() #root donne le nom a notre tkinter
root.title("Tu crypt ou tu decrypt ?")

Nbr = IntVar() #Permet de créer une text box pour saisir le decalage voulu et l'enregistrer dans Nbr
Nbr.set(0)
L = Label(root, text='Decalage de combien ?')
L.grid(column=0, row=0)
entry = Entry(root, textvariable=Nbr, width=30)
entry.grid(column=1, row=0)

PasEncoreCrypt = Text(root, wrap='word') #Cree une text box pour le texte pas encore crypte
PasEncoreCrypt.insert(END, "")
PasEncoreCrypt.grid(column=0, row=1)

ApresCrypt = Text(root, wrap='word') #Cree une text box pour le texte crypte
ApresCrypt.insert(END, "")
ApresCrypt.grid(column=1, row=1)

root.rowconfigure(0, weight=1)
root.columnconfigure(1, weight=1)


#Cree le bouton decryptage et permet de decrypter le texte saisit
button_Decryptage = Button(root, text="Tu veux decrypter ?", command=button_Decryptage_callback)
button_Decryptage.grid(row=2, column=1)


#Cree le bouton decryptage et permet de crypter le texte saisit
button_Cryptage = Button(root, text="Tu veux crypter ?", command=button_Cryptage_callback)
button_Cryptage.grid(row=2, column=0)

root.mainloop()