#Fonction qui va permettre de délimiter le tableau pour un cryptage et decryptage cyclique
#Le tableau est delimité pour prendre du A au z donc tous les caracteres de l'alphabet
def PourCyclique(CaractereMinimal='A ', CaractereMaximal='z'):
    TableauPourDelimitationAlphabet = []
    for Caractere in range(ord(CaractereMinimal), ord(CaractereMaximal) + 1):
        TableauPourDelimitationAlphabet.append(chr(Caractere))
    return TableauPourDelimitationAlphabet

#Fonction qui va retourner vrai ou faux si le caractere est bien dans notre alphabet
def CaractereDeMonAlphabet(Caractere):
    if Caractere in PourCyclique():
        return True
    else:
        return False  

#Fonction qui va permettre de décaler en fonction de l'alphabet prédéfini dans la
#fonction du haut et retournera donc le message si les conditions respectées
def PourDecaler(Caractere):
    MessageCode = Caractere
    CaractereMaximal = ord('z')
    CaractereMinimal = ord('A')
    while CaractereDeMonAlphabet(MessageCode) == False:
        ASCII = ord(MessageCode)
        if ASCII > CaractereMaximal:
            ASCII = ASCII % CaractereMaximal
            ASCII = ASCII + CaractereMinimal
        elif ASCII < CaractereMinimal:
            temp = ASCII - CaractereMinimal
            ASCII = CaractereMaximal - temp
        MessageCode = chr(ASCII)
    return MessageCode


#Fonction de cryptage qui va effectuer de facon cyclique chaque caractere additionne dans
#l'alphabet avec le nombre choisit du décalage
def Cryptage(Chaine, Nbr, Cyclique=False):
    MessageCode=""
    for Indice in range(len(Chaine)):
        if Cyclique == False:
            Caractere = chr(ord(Chaine[Indice])+Nbr)
        else:
            if CaractereDeMonAlphabet(Chaine[Indice]) == True:
                Caractere = PourDecaler(chr(ord(Chaine[Indice])+Nbr))
            else:
                Caractere = Chaine[Indice]
        MessageCode = MessageCode + Caractere
    return MessageCode      


#Fonction de cryptage qui va effectuer de facon cyclique chaque caractere soustrait dans
#l'alphabet avec le nombre choisit du décalage
def Decryptage(Chaine, Nbr, Cyclique=False):
    MessageCode=""
    for Indice in range(len(Chaine)):
        if Cyclique == False:
            Caractere = chr(ord(Chaine[Indice])-Nbr)
        else:
            if CaractereDeMonAlphabet(Chaine[Indice]) == True:
                Caractere = PourDecaler(chr(ord(Chaine[Indice])-Nbr))
            else:
                Caractere = Chaine[Indice]
        MessageCode = MessageCode + Caractere
    return MessageCode        


